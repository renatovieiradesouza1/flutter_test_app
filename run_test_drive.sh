#!/bin/sh
./chromedriver --allowed-ips --allowed-origins=* --whitelisted-ips --port=4444 --detach=true &
FOO_PID=$!
# nohup sh -c /app/chromedriver --whitelisted-ips &

flutter doctor
flutter pub get
flutter clean

# run test
flutter drive --driver=test_driver/main.dart --target=integration_test/app_test.dart -d web-server

kill $FOO_PID
